<?php

return [
    'base_url' => env('CAMPAIGN_URL'),
    'secret_key' => env('CAMPAIGN_SECRET_KEY', ''),
];