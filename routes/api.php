<?php

Route::post('login', 'Auth\ApiLoginController@login');
Route::middleware('auth:api')->post('check-number', 'PhoneController@checkNumber');
Route::middleware('auth:api')->post('coins', 'CoinController@pairs');
Route::middleware('auth:api')->get('get-campaign-by-id/{id}', 'CampaingController@getCampaignById');
if (config('geo-api.need_authorize')) {
    Route::middleware('auth:api')->get('geo-data', 'GeoIpController@geoData');
} else {
    Route::get('geo-data', 'GeoIpController@geoData');
}
