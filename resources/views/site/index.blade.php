<!doctype html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <title>Datasheet sender</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('assets/css/sheetjs.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('assets/css/jquery.handsontable.full.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('assets/css/samples.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('assets/css/alertify.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('assets/css/frontback.css') }}">
</head>
<body>
<div class="ui-main">
    <div class="ui-container-head">
        <div class="social-icon">
            <object data="{{ asset('assets/img/logosheet.svg') }}" type="image/svg+xml"></object>
        </div>
    </div>
    <div class="ui-container-controls">
        <div class="ui-container-form">
            {{ Form::open(['url' => 'upload', 'enctype' => 'multipart/form-data', 'id' => 'form_sheet']) }}
            <div class="ui-container-flex">
                <div>
                    <h2 class="h2-tags">Телефоны</h2>
                    <div class="selector_container" id="selectors_phone"></div>
                </div>
                <div>
                    <h2 class="h2-tags">Страны</h2>
                    <div class="selector_container" id="selectors_country"></div>
                </div>
            </div>
            <div>
                <div class="ui-container-droparea">
                    <input type="file" name="file" class="drop" id="drop" accept=".xls, .xlsx, .csv">
                </div>
                {{ Form::submit() }}
                {{ Form::close() }}
            </div>
        </div>

        <div id="buttons"></div>
        <div id="hot"></div>
    </div>
</div>
<script src="{{ asset('assets/vendor/alertify.js')}}"></script>
<script src="{{ asset('assets/vendor/jquery.min.js')}}"></script>
<script src="{{ asset('assets/vendor/jquery.handsontable.full.js')}}"></script>

<script src="{{ asset('assets/js/shim.js')}}"></script>
<script src="{{ asset('assets/js/xlsx.full.min.js')}}"></script>
<script src="{{ asset('assets/js/dropsheet.js')}}"></script>
<script src="{{ asset('assets/js/main.js')}}"></script>
<script src="{{ asset('assets/vendor/spin.js')}}"></script>

<script src="{{ asset('assets/js/frontback.js')}}"></script>
</body>
</html>
