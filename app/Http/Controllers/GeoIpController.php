<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeoIpController extends Controller
{
    public function geoData(Request $request)
    {
        return collect($request->server())->only([
            'GEOIP_COUNTRY_CODE',
            'GEOIP_COUNTRY_CODE3',
            'GEOIP_COUNTRY_NAME',
            'GEOIP_CITY_COUNTRY_CODE',
            'GEOIP_CITY_COUNTRY_CODE3',
            'GEOIP_CITY_COUNTRY_NAME',
            'GEOIP_REGION',
            'GEOIP_CITY',
            'GEOIP_POSTAL_CODE',
            'GEOIP_CITY_CONTINENT_CODE',
            'GEOIP_LATITUDE',
            'GEOIP_LONGITUDE',
        ]);
    }
}
