<?php

namespace App\Http\Controllers;

use App\Components\Coin\CoinHelper;
use App\Http\Requests\CoinPairs;

class CoinController extends Controller
{
    /** @var CoinHelper  */
    protected $coinHelper;

    public function __construct(CoinHelper $coinHelper)
    {
        $this->coinHelper = $coinHelper;
    }

    public function pairs(CoinPairs $request)
    {
        return $this->coinHelper->getPairs()->filterPairsByName($request->get('pairs'));
    }
}