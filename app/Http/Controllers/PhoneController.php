<?php

namespace App\Http\Controllers;

use App\Components\Phone\PhoneCheck;
use App\Components\Phone\PhoneChecker;
use App\Http\Requests\CheckNumber;
use Exception;

class PhoneController extends Controller
{
    protected $phoneChecker;

    public function __construct(PhoneChecker $phoneChecker)
    {
        $this->phoneChecker = $phoneChecker;
    }

    public function checkNumber(CheckNumber $request)
    {
        try {
            return $this->phoneChecker->checkNumber($request->number);
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function checkNumberExists(CheckNumber $request)
    {
        try {
            $result = $this->phoneChecker->checkNumber($request->number) instanceof PhoneCheck;
        } catch (Exception $e) {
            $result = false;
        } finally {
            return ['result' => $result];
        }
    }
}
