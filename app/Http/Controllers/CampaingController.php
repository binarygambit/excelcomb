<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CampaingController extends Controller
{
    public function getCampaignById($id)
    {
        $client = new Client([
            'base_uri' => config('campaing.base_url'),
        ]);

        try {
            $response = $client->post('getCampaignId', [
                'json' => [
                    'secretKey' => config('campaing.secret_key'),
                    'campaign_id' => (int)$id,
                ]
            ]);
        } catch (ClientException $clientException) {
            $response = $clientException->getResponse();
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'Something is going wrong. Try again later',
            ];
        }

        return $response->getBody()->getContents();
    }
}
