<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\ApiLogin;
use App\User;
use Illuminate\Routing\Controller;

class ApiLoginController extends Controller
{
    public function login(ApiLogin $request)
    {
        $user = User::whereEmail($request->email)->first();

        if ($user !== null && \Hash::check($request->password, $user->password)) {
            return ['token' => $user->api_token];
        }

        return ['error' => 'Wrong Credentials'];
    }
}