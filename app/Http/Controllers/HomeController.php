<?php

namespace App\Http\Controllers;

use App\Components\Sheet;
use App\Http\Requests\FileUpload;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('site.index');
    }

    public function upload(FileUpload $request)
    {
        $file = $request->file('file');
        (new Sheet($file, $request->get('sheet')))
            ->transform()
            ->download('xls');
    }
}
