<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileUpload extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'file' => 'required',
            'sheet' => 'array',
            'sheet.*' => 'array',
            'sheet.*.phone' => 'integer',
            'sheet.*.country' => 'integer',
        ];
    }
}
