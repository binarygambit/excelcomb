<?php

namespace App\Console\Commands;

use App\Components\Coin\CoinHelper;
use Illuminate\Console\Command;

class CoinsCache extends Command
{
    protected $signature = 'coins:cache';
    protected $description = 'Update coins cache';
    /** @var CoinHelper  */
    protected $coinsHelper;

    public function __construct(CoinHelper $coinHelper)
    {
        parent::__construct();
        $this->coinsHelper = $coinHelper;
    }


    public function handle()
    {
        $this->coinsHelper->cachePairs();
    }
}
