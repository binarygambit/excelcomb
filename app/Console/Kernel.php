<?php

namespace App\Console;

use App\Console\Commands\CoinsCache;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        CoinsCache::class,
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('coins:cache')->hourly();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
