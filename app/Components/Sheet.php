<?php

namespace App\Components;

use App\Components\Phone\PhoneParser;
use Excel;
use Exception;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Collections\CellCollection;
use Maatwebsite\Excel\Collections\RowCollection;
use Maatwebsite\Excel\Collections\SheetCollection;
use Maatwebsite\Excel\Writers\CellWriter;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


class Sheet
{
    /** @var UploadedFile */
    protected $file;
    /** @var array */
    protected $columnMap;
    /** @var PhoneParser */
    protected $phone;

    private $path;
    /** @var  SheetCollection */
    private $dataCollection;
    private $redCellsMap = [];
    private $rowsAlphabetical;

    public function __construct(UploadedFile $file, array $columnMap)
    {
        $this->file = $file;
        $this->path = $this->file->store('sheets');
        $this->columnMap = $columnMap;
        $this->phone = new PhoneParser;
        $this->rowsAlphabetical = range('A', 'Z');
    }

    public function transform(): LaravelExcelWriter
    {
        throw_unless($this->path, new FileException('Can\'t save file'));

        return $this->read()->write();
    }

    protected function read(): Sheet
    {
        $list = 0;
        /** @var SheetCollection $collection */
        $this->dataCollection = Excel::load(storage_path('app/' . $this->path))->get();

        if ($this->dataCollection instanceof SheetCollection) {
            $this->dataCollection = $this->dataCollection->map($this->getRowReadMapper($list));
        } elseif ($this->dataCollection instanceof RowCollection) {
            $rowIndex = 1;
            $this->dataCollection = $this->dataCollection->map($this->getCellReadMapper($list, $rowIndex));
        }

        return $this;
    }

    protected function write(): LaravelExcelWriter
    {
        $list = 0;
        /** @var LaravelExcelWriter $laravelExcelWriter */
        $laravelExcelWriter = Excel::create($this->file->getClientOriginalName(),
            function (LaravelExcelWriter $writer) use (&$list) {
                if ($this->dataCollection instanceof SheetCollection) {
                    $this->dataCollection->each($this->getRowWriteMapper($writer, $list));
                } elseif ($this->dataCollection instanceof RowCollection) {
                    $this->getRowWriteMapper($writer, $list)($this->dataCollection);
                }
            });

        return $laravelExcelWriter;
    }

    protected function getRowWriteMapper(LaravelExcelWriter $writer, &$list)
    {
        return function (RowCollection $rows) use ($writer, &$list) {
            $writer->sheet($rows->getTitle(), function (LaravelExcelWorksheet $sheet) use ($rows, &$list) {
                try {
                    $sheet->fromArray(
                        $rows->toArray(),
                        null,
                        'A1',
                        false,
                        false
                    );
                    $this->setColumnGreenBackground($sheet, $list, $rows->count());
                    $this->setListRedBackground($sheet, $list);
                } catch (Exception $e) {
                    $sheet->fromArray([]);
                }
                $list++;
            });
        };
    }

    protected function setColumnGreenBackground(LaravelExcelWorksheet $sheet, int $list, int $height)
    {
        $column = $this->rowsAlphabetical[$this->columnMap[$list]['phone']];
        $sheet->cells("{$column}1:{$column}{$height}", function (CellWriter $cells) {
            $cells->setBackground('#09FF00');
        });
    }

    protected function setListRedBackground(LaravelExcelWorksheet $sheet, int $list)
    {
        if (!isset($this->redCellsMap[$list])) {
            return;
        }

        $column = $this->rowsAlphabetical[$this->columnMap[$list]['phone']];

        foreach ($this->redCellsMap[$list] as $rowIndex) {
            $sheet->cell("{$column}{$rowIndex}", function (CellWriter $cell) {
                $cell->setBackground('#ff0000');
            });
        }
    }

    protected function getRowReadMapper(&$list)
    {
        return function (RowCollection $row) use (&$list) {
            $title = $row->getTitle();
            $rowIndex = 1;
            $row = $row->map($this->getCellReadMapper($list, $rowIndex));
            $row->setTitle($title);
            $list++;

            return $row;
        };
    }

    protected function getCellReadMapper(&$list, &$rowIndex)
    {
        return function (CellCollection $cell) use (&$list, &$rowIndex) {
            try {
                $number = $cell->get($this->columnMap[$list]['phone']);
                $country = $cell->get($this->columnMap[$list]['country']);

                if ($country === null || $number === null) {
                    throw new Exception('Empty cell');
                }

                $phone = $this->phone->getInternationalNumber(
                    $number,
                    $country
                );

                if ($this->phone->isValid()) {
                    $cell[$this->columnMap[$list]['phone']] = $phone;
                    $cell[$this->columnMap[$list]['country']] = $this->phone->getCountry();
                } else {
                    $this->redCellsMap[$list][] = $rowIndex;
                }
            } catch (Exception $e) {
                $this->redCellsMap[$list][] = $rowIndex;
            }
            $rowIndex++;

            return $cell;
        };
    }
}
