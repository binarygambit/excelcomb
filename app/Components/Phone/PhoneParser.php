<?php

namespace App\Components\Phone;

use App\Components\Country;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class PhoneParser
{
    /** @var PhoneNumberUtil  */
    protected $phoneUtil;
    /** @var Country  */
    protected $countryUtil;
    protected $removeChars = [
        ' ',
        '(',
        ')',
        '+',
        '-',
    ];

    private $number;
    private $country;
    /** @var PhoneNumber */
    private $phoneNumber;

    public function __construct()
    {
        $this->phoneUtil = PhoneNumberUtil::getInstance();
        $this->countryUtil = new Country;
    }

    protected function setCountry($country)
    {
        $this->country = $this->countryUtil->searchCodeByCountry($country);

        return $this;
    }

    /**
     * @param $number
     * @param $country
     * @return mixed
     * @throws \libphonenumber\NumberParseException
     */
    public function getInternationalNumber($number, $country)
    {
        return $this->setNumber($number)
            ->setCountry($country)
            ->toInt()
            ->removeChars()
            ->removeNames()
            ->parse()
            ->getNumber();
    }

    /**
     * @return $this
     * @throws \libphonenumber\NumberParseException
     */
    protected function parse()
    {
        $this->phoneNumber = $this->phoneUtil->parse($this->number, $this->country);
        $this->number = $this->phoneUtil->format($this->phoneNumber, PhoneNumberFormat::INTERNATIONAL);

        return $this;
    }

    public function isValid()
    {
        return $this->phoneUtil->isValidNumberForRegion($this->phoneNumber, $this->country);
    }
    
    public function getCountry() {
        return $this->country;
    }
    
    protected function toInt()
    {
        if (is_float($this->number)) {
            $this->number = (int)$this->number;
        }

        return $this;
    }

    protected function removeChars()
    {
        if (is_string($this->number)) {
            foreach ([' ', '(', ')', '+', '-'] as $char) {
                $this->number = str_replace($char, '', $this->number);
            }
        }

        return $this;
    }

    protected function removeNames()
    {
        preg_match_all('/\d*/', $this->number, $matches);
        $this->number = array_filter($matches[0])[0];

        return $this;
    }

    protected function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    protected function getNumber()
    {
        return $this->number;
    }
}
