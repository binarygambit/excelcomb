<?php

namespace App\Components\Phone;

use Exception;

class PhoneChecker
{
    /** @var PhoneCheckerRequest  */
    protected $request;

    public function __construct()
    {
        $this->request = new PhoneCheckerRequest;
    }

    /**
     * @param string $phoneNumber
     * @return PhoneCheck
     * @throws Exception
     */
    public function checkNumber(string $phoneNumber): PhoneCheck
    {
        $phoneCheck = $this->request->checkNumber($phoneNumber);

        if (isset($phoneCheck['error'])) {
            throw new Exception($phoneCheck['error']);
        }

        return PhoneCheck::fromArray($phoneCheck);
    }
}