<?php

namespace App\Components\Phone;


use Illuminate\Contracts\Support\Arrayable;

class PhoneCheck implements Arrayable
{
    /** @var string */
    public $countryCode;
    /** @var string */
    public $countryIsoCode;
    /** @var string */
    public $carrier;
    /** @var bool */
    public $isMobile;
    /** @var string */
    public $e164Format;
    /** @var string */
    public $formatting;

    /**
     * PhoneCheck constructor.
     * @param $countryCode
     * @param $countryIsoCode
     * @param $carrier
     * @param $isMobile
     * @param $e164Format
     * @param $formatting
     */
    public function __construct($countryCode, $countryIsoCode, $carrier, $isMobile, $e164Format, $formatting)
    {
        $this->countryCode = (int)$countryCode;
        $this->countryIsoCode = $countryIsoCode;
        $this->carrier = $carrier;
        $this->isMobile = $isMobile;
        $this->e164Format = $e164Format;
        $this->formatting = $formatting;
    }

    public static function fromArray(array $phoneCheck)
    {
        return new self(
            $phoneCheck['country_code'],
            $phoneCheck['country_iso_code'],
            $phoneCheck['carrier'],
            $phoneCheck['is_mobile'],
            $phoneCheck['e164_format'],
            $phoneCheck['formatting']
        );
    }

    public static function fromObject($phoneCheck)
    {
        return new self(
            $phoneCheck->country_code,
            $phoneCheck->country_iso_code,
            $phoneCheck->carrier,
            $phoneCheck->is_mobile,
            $phoneCheck->e164_format,
            $phoneCheck->formatting
        );
    }

    public function toArray()
    {
        return [
            'country_code' => $this->countryCode,
            'country_iso_code' => $this->countryIsoCode,
            'carrier' => $this->carrier,
            'is_mobile' => $this->isMobile,
            'e164_format' => $this->e164Format,
            'formatting' => $this->formatting,
        ];
    }
}