<?php

namespace App\Components\Phone;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class PhoneCheckerRequest
{
    /** @var Client  */
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.checkmobi.com/v1/',
        ]);
    }

    public function checkNumber(string $phoneNumber): array
    {
        try {
            $response = $this->client->post('checknumber', [
                'headers' => [
                    'Authorization' => config('phone-check.api-key'),
                ],
                'json' => [
                    'number' => $phoneNumber,
                ],
            ]);
        } catch (ClientException $e) {
            return json_decode($e->getResponse()->getBody(), true);
        }

        throw_unless($response->getStatusCode() === 200, new Exception('Error response from checkmobi.com'));

        return json_decode($response->getBody(), true);
    }
}