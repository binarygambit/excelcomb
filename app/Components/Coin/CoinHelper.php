<?php

namespace App\Components\Coin;

use Cache;

class CoinHelper
{
    /** @var CoinRequest  */
    protected $request;
    protected $cacheKey;

    public function __construct()
    {
        $this->request = new CoinRequest;
        $this->cacheKey = 'coins';
    }

    public function getCoins(int $limit = 100): array
    {
        return $this->request->ticker()->limit($limit)->get();
    }

    public function makePairs(): CoinPairCollection
    {
        return CoinCollection::fromObjects($this->getCoins())->matchPairs();
    }

    public function cachePairs(CoinPairCollection $coinPairCollection = null)
    {
        Cache::put($this->cacheKey, $coinPairCollection ?? $this->makePairs(), 60);
    }

    public function getPairs(): CoinPairCollection
    {
        return Cache::get($this->cacheKey, function () {
            $coinPairs = $this->makePairs();
            $this->cachePairs($coinPairs);

            return $coinPairs;
        });
    }
}
