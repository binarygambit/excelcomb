<?php

namespace App\Components\Coin;

use Exception;
use GuzzleHttp\Client;
use InvalidArgumentException;

class CoinRequest
{
    protected $client;
    protected $endpoint;
    protected $options = [];

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.coinmarketcap.com/v1/',
        ]);
    }

    public function ticker($currency = null)
    {
        $this->endpoint = 'ticker' . ($currency ? '/'.$currency : '') . '/';

        return $this;
    }

    public function global()
    {
        $this->endpoint = 'global/';
    }

    public function limit(int $limit)
    {
        $this->options['limit'] = $limit;

        return $this;
    }

    public function convert(string $convert)
    {
        $this->options['convert'] = $convert;

        return $this;
    }

    public function get()
    {
        throw_unless($this->endpoint, new InvalidArgumentException('No endpoint'));
        $response = $this->client->get($this->endpoint, ['query' => $this->options]);
        throw_unless($response->getStatusCode() === 200, new Exception('Error response from coinmarketcap.com'));

        return json_decode($response->getBody()->getContents());
    }
}