<?php

namespace App\Components\Coin;


use Illuminate\Support\Collection;

class CoinCollection extends Collection
{
    public function __construct($items = [])
    {
        parent::__construct($this->validate(...$items));
    }

    protected function validate(Coin ...$coins)
    {
        return $coins;
    }

    public function matchPairs(): CoinPairCollection
    {
        $pairsCollection = new CoinPairCollection;

        $this->each(function (Coin $jay) use ($pairsCollection) {
            $this->each(function (Coin $bob) use ($jay, $pairsCollection) {
                if ($jay->name != $bob->name) {
                    $pairsCollection->pushPair(new CoinPair($jay, $bob));
                }
            });
        });

        return $pairsCollection;
    }

    public static function fromObjects(array $response)
    {
        return new self(collect($response)->map(function ($coin) {
            return Coin::fromObject($coin);
        }));
    }
}