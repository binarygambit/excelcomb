<?php

namespace App\Components\Coin;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class CoinPair implements Arrayable
{
    /** @var Coin  */
    protected $jay;
    /** @var Coin  */
    protected $bob;

    public function __construct(Coin $jay, Coin $bob)
    {
        $this->jay = $jay;
        $this->bob = $bob;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->getName(),
            'plain_name' => $this->getPlainName(),
            'rate' => round($this->getRate(), 2),
            'price_first' => $this->jay->price_usd,
            'price_second' => $this->bob->price_usd,
            'percent_change_1h' => round($this->getPercentChange1h(), 2),
            'percent_change_24h' => round($this->getPercentChange24h(), 2),
            'percent_change_7d' => round($this->getPercentChange7d(), 2),
            'last_updated' => Carbon::now()->timestamp,
        ];
    }

    public function getName()
    {
        return "{$this->jay->symbol}/{$this->bob->symbol}";
    }

    public function getPlainName()
    {
        return "{$this->jay->name}/{$this->bob->name}";
    }

    public function getRate()
    {
        return $this->jay->price_usd / $this->bob->price_usd;
    }

    public function getPercentChange1h()
    {
        $previousRate = $this->jay->getPreviousPrice1h() / $this->bob->getPreviousPrice1h();

        return $this->getPercentChange($previousRate);
    }

    public function getPercentChange24h()
    {
        $previousRate = $this->jay->getPreviousPrice24h() / $this->bob->getPreviousPrice24h();

        return $this->getPercentChange($previousRate);
    }

    public function getPercentChange7d()
    {
        $previousRate = $this->jay->getPreviousPrice7d() / $this->bob->getPreviousPrice7d();

        return $this->getPercentChange($previousRate);
    }

    protected function getPercentChange($previousRate)
    {
        return 100 - $this->getRate() * 100 / $previousRate;
    }
}