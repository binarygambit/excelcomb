<?php

namespace App\Components\Coin;

use Carbon\Carbon;

class Coin
{
    /** @var string */
    public $id;
    /** @var string */
    public $name;
    /** @var string */
    public $symbol;
    /** @var int */
    public $rank;
    /** @var float */
    public $price_usd;
    /** @var float */
    public $price_btc;
    /** @var float */
    public $volume_usd_24h;
    /** @var float */
    public $market_cap_usd;
    /** @var float */
    public $available_supply;
    /** @var float */
    public $total_supply;
    /** @var float */
    public $percent_change_1h;
    /** @var float */
    public $percent_change_24h;
    /** @var float */
    public $percent_change_7d;
    /** @var Carbon */
    public $last_updated;

    /**
     * Coin constructor.
     * @param string $id
     * @param string $name
     * @param string $symbol
     * @param int $rank
     * @param float $price_usd
     * @param float $price_btc
     * @param float $volume_usd_24h
     * @param float $market_cap_usd
     * @param float $available_supply
     * @param float $total_supply
     * @param float $percent_change_1h
     * @param float $percent_change_24h
     * @param float $percent_change_7d
     * @param int $last_updated
     */
    public function __construct(
        $id,
        $name,
        $symbol,
        $rank,
        $price_usd,
        $price_btc,
        $volume_usd_24h,
        $market_cap_usd,
        $available_supply,
        $total_supply,
        $percent_change_1h,
        $percent_change_24h,
        $percent_change_7d,
        $last_updated
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->symbol = $symbol;
        $this->rank = (int)$rank;
        $this->price_usd = (float)$price_usd;
        $this->price_btc = (float)$price_btc;
        $this->volume_usd_24h = (float)$volume_usd_24h;
        $this->market_cap_usd = (float)$market_cap_usd;
        $this->available_supply = (float)$available_supply;
        $this->total_supply = (float)$total_supply;
        $this->percent_change_1h = (float)$percent_change_1h;
        $this->percent_change_24h = (float)$percent_change_24h;
        $this->percent_change_7d = (float)$percent_change_7d;
        $this->last_updated = Carbon::createFromTimestamp($last_updated);
    }

    public static function fromArray(array $coin)
    {
        return new self(
            $coin['id'],
            $coin['name'],
            $coin['symbol'],
            $coin['rank'],
            $coin['price_usd'],
            $coin['price_btc'],
            $coin['24h_volume_usd'],
            $coin['market_cap_usd'],
            $coin['available_supply'],
            $coin['total_supply'],
            $coin['percent_change_1h'],
            $coin['percent_change_24h'],
            $coin['percent_change_7d'],
            $coin['last_updated']
        );
    }

    public static function fromObject($coin)
    {
        return new self(
            $coin->id,
            $coin->name,
            $coin->symbol,
            $coin->rank,
            $coin->price_usd,
            $coin->price_btc,
            $coin->{'24h_volume_usd'},
            $coin->market_cap_usd,
            $coin->available_supply,
            $coin->total_supply,
            $coin->percent_change_1h,
            $coin->percent_change_24h,
            $coin->percent_change_7d,
            $coin->last_updated  
        );
    }

    public function getPreviousPrice1h()
    {
        return $this->getPreviousPrice($this->percent_change_1h);
    }

    public function getPreviousPrice24h()
    {
        return $this->getPreviousPrice($this->percent_change_24h);
    }

    public function getPreviousPrice7d()
    {
        return $this->getPreviousPrice($this->percent_change_7d);
    }

    protected function getPreviousPrice($percent)
    {
        return $this->price_usd * ((100 + $percent) / 100);
    }
}