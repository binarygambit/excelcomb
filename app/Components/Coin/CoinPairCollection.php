<?php

namespace App\Components\Coin;

use Illuminate\Support\Collection;

class CoinPairCollection extends Collection
{
    public function pushPair(CoinPair $coinPair)
    {
        return $this->push($coinPair);
    }

    public function filterPairsByName(array $pairNames)
    {
        return $this->filter(function (CoinPair $coinPair) use ($pairNames) {
            return in_array($coinPair->getName(), $pairNames);
        });
    }
}