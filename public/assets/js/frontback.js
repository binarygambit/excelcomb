$(document).ready(function () {
    $("#form_sheet input[type='submit']").click(function (e) {
        e.preventDefault();
        if ($("#drop").val() != 0) {
            $("#form_sheet").submit();
        }
        else {
            alert("Добавьте необходимый файл");
        }
    });


    $("#drop").on("dragover", function(event) {
        event.preventDefault();
        event.stopPropagation();
        $(".ui-container-droparea").css("background-color","#0d3625");
    });
    $("#drop").on("dragleave", function(event) {
        event.preventDefault();
        event.stopPropagation();
        $(".ui-container-droparea").css("background-color","transparent");
    });
    var activeSheet = 0;

    $(document).on("focus", "#selectors_phone select", function (e) {
        if ($(this).hasClass("completed") && !$(this).hasClass('wrote')) {
            activeSheet = $(this).attr("data-id");
            WriteIntoSelect(activeSheet, "#selectors_phone select");
            $(this).addClass("wrote");
        }
    });

    $(document).on("focus", "#selectors_country select", function (e) {
        if ($(this).hasClass("completed") && !$(this).hasClass('wrote')) {
            activeSheet = $(this).attr("data-id");
            WriteIntoSelect(activeSheet, "#selectors_country select");
            $(this).addClass("wrote");
        }
    });

    //Select active sheet
    $(document).on("click", "button", function () {
        activeSheet = $(this).attr("data-id");
        //Write data in 'select' element
        WriteIntoSelect(activeSheet, "#selectors_phone select");
        WriteIntoSelect(activeSheet, "#selectors_country select");
    });

    (function HandsontableObserveChanges() {

        //Handsontable.PluginHooks.add('beforeRender', beforeInit);
        Handsontable.PluginHooks.add('afterRender', init);

        function init() {
            CreateSelectors();
            //On sheet load, write data in first 'select' element
            WriteIntoSelect(0, "#selectors_phone select");
            WriteIntoSelect(0, "#selectors_country select");
        }
    })();

    //FN Parse table and write col names into 'select' element
    function WriteIntoSelect(sheetID, iSelector) {
        if ($(iSelector).eq(sheetID).find("option:not(option:first-child)").length <= 0) {
            for (var i = 0; i < $(".wtSpreader thead tr th:not(th:first-child)").length; i++) {
                $(iSelector).eq(sheetID).append("<option value='" + i + "'>" + $(".wtSpreader thead tr th").eq(i + 1).find('span').text() + "</option>")
            }
        }
    }

    function CreateSelectors() {
        var iContainer = "#selectors_phone";
        if ($(iContainer + " select").length <= 0) {
            if ($(iContainer + " select").length <= 0) {
                for (var i = 0; i < $("#buttons button").length; i++) {
                    $(iContainer).append("<p>Колонка Phone для таблицы: " + $("#buttons button").eq(i).attr("text") + "</p><select data-id='" + i + "' name='sheet[" + $("#buttons button").eq(i).attr("data-id") + "][phone]'></select>")
                    $(iContainer + " select").eq(i).append("<option disabled selected>Для таблицы " + $("#buttons button").eq(i).attr("text") + "</option>")
                }
            }
        }
        iContainer = "#selectors_country";
        if ($(iContainer + " select").length <= 0) {
            if ($(iContainer + " select").length <= 0) {
                for (var i = 0; i < $("#buttons button").length; i++) {
                    $(iContainer).append("<p>Колонка Country для таблицы: " + $("#buttons button").eq(i).attr("text") + "</p><select data-id='" + i + "' name='sheet[" + $("#buttons button").eq(i).attr("data-id") + "][country]'></select>")
                    $(iContainer + " select").eq(i).append("<option disabled selected>Для таблицы " + $("#buttons button").eq(i).attr("text") + "</option>")
                }
            }
        }
    }

});
